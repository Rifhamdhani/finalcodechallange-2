package ch.service;

import java.util.List;

interface ReadWriteInterafce {

    void generateOperation(String txtFile);

    void generateGroup(String txtFile, int delimiter);

    List<List<String>> readFIle(String fileName, String delimiter);

}
